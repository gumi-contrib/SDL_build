#!/usr/bin/env python3

import argparse
import os.path
import platform
import shutil
import subprocess

script_dir = os.path.dirname(os.path.realpath(__file__))

parser = argparse.ArgumentParser(description="Build project")
parser.add_argument(
    "--build-type",
    choices=["Release", "Debug"],
    dest="build_type",
    default="Debug",
    help="The build type, Release or Debug")

args = parser.parse_args()

lib_output = os.path.join(script_dir, "output", args.build_type)
lib_output_debug = os.path.join(script_dir, "output", "Debug")
lib_output_release = os.path.join(script_dir, "output", "Release")

cmake_args = [
    # CMake config
    "-DCMAKE_BUILD_TYPE={}".format(args.build_type),
    "-DCMAKE_LIBRARY_OUTPUT_DIRECTORY='{}'".format(lib_output),
    "-DCMAKE_LIBRARY_OUTPUT_DIRECTORY_DEBUG='{}'".format(lib_output_debug),
    "-DCMAKE_LIBRARY_OUTPUT_DIRECTORY_RELEASE='{}'".format(lib_output_release),
    "-DCMAKE_ARCHIVE_OUTPUT_DIRECTORY='{}'".format(lib_output),
    "-DCMAKE_ARCHIVE_OUTPUT_DIRECTORY_DEBUG='{}'".format(lib_output_debug),
    "-DCMAKE_ARCHIVE_OUTPUT_DIRECTORY_RELEASE='{}'".format(lib_output_release),
    "--no-warn-unused-cli",
    # Platform config
    # Library config
    "-DSDL_STATIC=TRUE",
    "-DSDL_STATIC_PIC=TRUE",
    "-DSDL_SHARED=FALSE",
    "-DSDL_AUDIO_ENABLED_BY_DEFAULT=FALSE",
    "-DSDL_RENDER_ENABLED_BY_DEFAULT=FALSE",
    "-DSDL_THREADS_ENABLED_BY_DEFAULT=FALSE",
    "-DSDL_TIMERS_ENABLED_BY_DEFAULT=FALSE",
]

if __name__ == "__main__":
    cmake_path = shutil.which("cmake")
    source_dir = os.path.join(script_dir, "..", "..", "sdl")
    # Generate project
    generate_command = [cmake_path, source_dir] + cmake_args
    subprocess.run(generate_command, cwd=script_dir, check=True)
    # Execute build
    build_command = [cmake_path, "--build", script_dir,
                     "--config", args.build_type]
    subprocess.run(build_command, cwd=script_dir, check=True)
    # Copy include to library output
    try:
        shutil.rmtree(os.path.join(lib_output, "include"))
    except FileNotFoundError:
        pass
    shutil.copytree(os.path.join(script_dir, "include"),
                    os.path.join(lib_output, "include"))
